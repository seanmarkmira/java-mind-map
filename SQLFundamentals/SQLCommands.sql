-- SELECT Keyword in MySQL
-- The order is IMPORTANT SELECT-FROM-WHERE-ORDER BY
-- New lines does not matter for the SQL Statement

-- Sample SELECT statement
SELECT * FROM customers WHERE customer_id = 10 ORDER BY first_name;
SELECT name, unit_price, unit_price * 1.1 AS 'new price' FROM products;

-- Select Statement with operations (* + - % /)
SELECT first_name, last_name, points, (points + 10) * 100 AS 'discount factor' FROM customers;

-- Selecting UNIQUE information about the select statament
SELECT DISTINCT state FROM customers;

-- WHERE clause (>, >=, <, <=, =, != or <>)
SELECT * FROM Customers WHERE points > 3000;
SELECT * FROM Customers WHERE state <> 'va'; -- <> and != is same
SELECT * FROM Customers WHERE birth_date > '1990-01-01'; -- This is for the date
SELECT * FROM sql_store.orders WHERE order_date >= '2019-01-01'; -- exercise

-- AND OR NOT Operator
SELECT * FROM Customers WHERE birth_date > '1990-01-01' AND points > 1000;
-- IMPORTANT: in logical operation AND is evaluated first
SELECT * FROM Customers WHERE birth_date > '1990-01-01' OR points > 1000 AND state = 'va';
SELECT * FROM Customers WHERE birth_date > '1990-01-01' OR (points > 1000 AND state = 'va'); -- () is for better readability
SELECT * FROM Customers WHERE NOT birth_date > '1990-01-01' OR points > 1000; -- NOT condition in WHERE
-- NOTE on NOT: When using NOT keyword, you are now operating to where from > to <= and OR to AND. Remember, >/< is <=/>=
SELECT * FROM order_items WHERE order_id = 6 AND unit_price * quantity > 30; -- exercise, we can have arithmetic not just in select statement

-- IN Operator
SELECT * FROM customers WHERE state = 'VA' OR state = 'GA' OR state = 'FL'; -- Can be change to IN
SELECT * FROM customers WHERE state IN ('VA', 'FL', 'GA');
SELECT * FROM products WHERE quantity_in_stock IN (49, 38, 72); -- Exercise

-- Between Operator
SELECT * FROM customers WHERE points >= 1000 AND points <= 3000;
SELECT * FROM customers WHERE points BETWEEN 1000 AND 3000;
SELECT * FROM customers WHERE birth_date BETWEEN '1990-01-01' AND '2000-01-01'; -- exercise

-- LIKE Operator
SELECT * FROM customers WHERE last_name LIKE 'b____y'; -- _ means single char, % means any char
SELECT * FROM customers WHERE address LIKE '%TRAIL%' OR address LIKE '%AVENUE%'; -- Exercise
SELECT * FROM customers WHERE phone LIKE '%9'; -- Exercise

-- REGEXP
SELECT * FROM customers WHERE last_name REGEXP '^field|mac|rose';

-- IS NULL/IS NOT NULL
SELECT * FROM customers WHERE phone IS NOT NULL;
SELECT * FROM orders WHERE shipper_id IS NULL; -- Exercise

-- ORDER BY
SELECT * FROM customers ORDER BY state, first_name; -- if same state then sort by first_name
SELECT order_id, product_id, quantity, unit_price FROM order_items WHERE order_id = 2 ORDER BY quantity * unit_price DESC;

-- Limit
SELECT * FROM customers LIMIT 300;
SELECT * FROM customers LIMIT 6, 3;
SElECT * FROM customers ORDER BY points DESC LIMIT 3;

-- NOTES!
-- You cannot use just STRING in OR/AND condition, needs to be evaluated to a boolean
-- The idea where there is a lot of operator in WHERE clause is that we have to set the condition to GET/UPDATE/INSERT/DELETE

-- Combining multiple tables
SELECT order_id, orders.customer_id, last_name FROM orders JOIN customers ON customers.customer_id = orders.customer_id;
SELECT order_id, o.product_id, p.name, quantity, o.unit_price FROM order_items o JOIN products p ON p.product_id = o.product_id; -- exercise

-- Combining multiple DB
SELECT * FROM order_items oi JOIN sql_inventory.products p ON oi.product_id = p.product_id;

-- Self Join
use sql_hr;
SELECT * from employees;
SELECT e.employee_id, e.first_name, m.first_name AS 'Manager First Name' FROM employees e JOIN employees m ON e.reports_to = m.employee_id;
use sql_store;

SELECT o.order_id, o.order_date, c.first_name, c.last_name, os.name as STATUS FROM orders o 
JOIN customers c ON o.customer_id = c.customer_id 
JOIN order_statuses os ON o.status = os.order_status_id;
-- TIP: Basically, we are joining 2 tables' PK and FK and then just selecting the data presented

-- Exercise
-- Payments to be join with payment method and clients
use sql_invoicing;
SELECT p.date, p.amount, c.name, pm.name FROM payments p JOIN payment_methods pm ON pm.payment_method_id = p.payment_method JOIN clients c ON p.client_id = c.client_id; -- Exercise
-- TIP: Always start with, what are we JOINING and ON what bases (PK + FK), and then select the column to show

-- Composite JOIN - Where COMPOSITE is a PK that is more than 1 column
-- The idea here is that we have a COMPOSITE, where there are 2 FK, we need to utilize JOIN to uniquely identify the row
-- Both of the PRIMARY KEY of the COMPOSITE table must be considered, See example in order_items in sql_store

-- A sample of a compound join condition (AND condition on JOIN condition), unfortunately, it returns no data. We should be able to understand what it is trying to do
-- We need more example of compound join condition to understand this further
use sql_store;
SELECT * FROM order_items oi JOIN order_item_notes oin ON oi.order_id = oin.order_id AND oi.product_id = oin.product_id; -- Does not return anything
use sql_invoicing;

-- OUTER JOIN (LEFT OR RIGHT) The OUTER keyword is optional
-- The concept of this is that we are getting data basing from the LEFT/RIGHT table + the join condition
-- For example, if we use LEFT JOIN, we are going to get all of the data in the customers table + the condition set in JOIN (ON) condition
SELECT c.customer_id, c.first_name, o.order_id FROM customers c LEFT JOIN orders o ON c.customer_id = o.customer_id ORDER BY c.customer_id;
-- EXERCISE - OUTER JOIN (Left/Right)
SELECT p.product_id, p.name, oi.quantity FROM products p LEFT JOIN order_items oi ON p.product_id = oi.product_id;

-- OUTER JOIN with Multiple Tables
-- Good practice, always use the LEFT JOIN always in your queries
SELECT c.customer_id, c.first_name, o.order_id, sh.name AS shipper FROM customers c LEFT JOIN orders o ON c.customer_id = o.customer_id LEFT JOIN shippers sh ON o.shipper_id = sh.shipper_id ORDER BY c.customer_id;
-- Exercise
SELECT o.order_date, o.order_id, c.first_name, s.name AS shipper, os.name AS status FROM orders o JOIN customers c ON o.customer_id = c.customer_id LEFT JOIN shippers s ON o.shipper_id = s.shipper_id JOIN order_statuses os ON o.status = os.order_status_id;
-- WHAT WE LEARNED! Use this mindset when joining multiple table:
-- 1.) Where are we going to get the columns?
-- 2.) What is the FK-PK relationship? Use that on ON condition
-- 3.) If we are combining multiple tables, the result of the previous JOIN statement will be your basis on your next JOIN (left/right) statement
-- 4.) If we need to consider the null values, then think which result of the join statement will be our basis.

-- SELF OUTER JOIN
-- The concept is still the same but we will be using the same table to join it
USE sql_hr;
SELECT e.employee_id, e.first_name, m.first_name AS manager FROM employees e LEFT JOIN employees m ON e.reports_to = m.employee_id;

-- The USING Clause
-- If you have the same PK/FK name on joining tables, then you can just use the USING (), clause to make it more readable
use sql_store;
SELECT o.order_id, c.first_name, sh.name AS shipper FROM orders o JOIN customers c USING (customer_id) LEFT JOIN shippers sh USING (shipper_id);
-- Sample of composite table joined together through USING clause - seems like something wrong with the example given by tutorial
SELECT * FROM order_items oi JOIN order_item_notes oin USING (order_id, product_id);
-- Exercise
SELECT p.date, c.name, p.amount, pm.name FROM clients c JOIN payments p USING (client_id) JOIN payment_methods pm ON pm.payment_method_id = p.payment_method;

-- NATURAL JOINS
-- Simpler way to join 2 tables - can give unexpected results
-- It is not advisable as we are giving the database engine guessing the join
SELECT o.order_id, c.first_name FROM orders o NATURAL JOIN customers c;

-- CROSS JOINS
-- To combine all records on the first table to the records of the second table
-- Explicit Syntax Sample
SELECT c.first_name AS customer, p.name AS product FROM customers c CROSS JOIN products p ORDER BY c.first_name;
-- Implicit Syntax Sample - not recommendable
SELECT c.first_name AS customer, p.name AS product FROM customers c, products p ORDER BY c.first_name;
-- Exercise
-- Shippers and Products - implicit and explicit
SELECT * FROM products CROSS JOIN shippers;
SELECT * FROM products, shippers;

-- END OF JOINS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- START OF UNIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-- Unions
-- Joins is to columns from multiple table, Unions is to rows from multiple table
-- By using the union, we can combine the result of a specific query to a single result set
-- NOTE: In using UNIONS, the first query and second query must have the same number columns in the SELECT statement
-- NOTE: The first query will always be on top and then followed by the other queries
SELECT order_id, order_date, 'Active' AS status FROM orders WHERE order_date >= '2019-01-01'
UNION
SELECT order_id, order_date, 'Archived' AS status FROM orders WHERE order_date < '2019-01-01';

-- A sample of UNION of combining multiple tables
SELECT first_name 
FROM customers 
UNION 
SELECT name
FROM shippers;
-- Exercise
SELECT customer_id, first_name, points, 'Gold' AS type FROM customers WHERE points > 3000
UNION
SELECT customer_id, first_name, points, 'Silver' AS type FROM customers WHERE points >= 2000 AND points <= 3000
UNION
SELECT customer_id, first_name, points, 'Bronze' AS type FROM customers WHERE points < 2000 ORDER BY first_name;

-- Column Attribute
-- Data Type in the column:
-- VARCHAR - will only store the characters. vs CHAR - MySQL will add additional characters to fill the whole space, for example, CHAR(50) and you consume only 5, MySQL will make 45 additional characters
-- NN - Not Null, can accept null values?
-- AI - Auto Increment, often used in PK. If user will not supply values, MySQL will enter one
-- Default/Expression - Default Values if not supplied by the user

-- INSERTING a Single Row
-- You can use DEFAULT, if the table has a default value setup
-- You can also user NULL, if the NN is not checked in the column attribute
INSERT INTO customers (first_name, last_name, birth_date, address, city, state) VALUES ('John', 'Smith', '1990-01-01','address','city','CA');
-- INSERTING Multiple Rows
INSERT INTO shippers (name) VALUES ('Shipper1'), ('Shipper2'), ('Shipper3');
-- EXERCISE
INSERT INTO products (name, quantity_in_stock, unit_price) VALUES ('Discovery Analysis', '99', 15.00), ('Plan for Plan', '99', 10.00), ('Design and Analysis', '99', 20.00);

-- Inserting Hierarchical Rows (Inserting multiple tables)
-- USE CASE: When you have parent-child relationship of the table
INSERT INTO orders (customer_id, order_date, status) VALUES (1, '2019-01-02', 1);
INSERT INTO order_items (order_id, product_id, quantity, unit_price) VALUES (LAST_INSERT_ID(), 1, 1, 2.95), (LAST_INSERT_ID(), 2, 1, 3.95);

-- This statement will use the last inserted ID in MySQL
SELECT LAST_INSERT_ID();

-- CREATING A COPY OF A TABLE
-- Take note that PK and AI in the column attribute is not followed anymore
CREATE TABLE orders_archived AS SELECT * FROM orders;

-- Sample of utilizing a subquery
-- Usually, a subquery is a SELECT statement
INSERT INTO orders_archived SELECT * FROM orders WHERE order_date < '2019-01-01';

-- EXERCISE
CREATE TABLE invoices_archived SELECT c.name AS client, i.number, i.invoice_total, i.payment_total, i.invoice_date, i.due_date, i.payment_date FROM invoices i JOIN clients c USING (client_id) WHERE i.payment_date IS NOT NULL;

-- UPDATING A SINGLE ROW
UPDATE invoices SET payment_total = DEFAULT, payment_date = NULL WHERE invoice_id = 1;
UPDATE invoices SET payment_total = invoice_total * 0.5, payment_date = due_date WHERE invoice_id = 3;

-- UPDATING MULTIPLE ROWS
UPDATE INVOICES SET payment_total = invoice_total * 0.5 , payment_date = due_date WHERE client_id IN (3,4);
-- EXERCISE
UPDATE customers SET points = points + 50 WHERE birth_date < '1990-01-01';

-- USING SUBQUERIES IN UPDATES
-- Remember that subqueries are just select statements for a sql statement
-- As you can see, we used IN operator since the result of the subquery is multiple rows
-- If the result of subquery is just 1 single row you may use =
UPDATE invoices SET payment_total = invoice_total * .50, payment_date = due_date WHERE client_id IN (SELECT client_id FROM clients WHERE state IN ('CA', 'NY'));
-- EXERCISE
UPDATE orders SET comments = 'GOLD' WHERE customer_id IN (SELECT customer_id FROM customers WHERE points > 3000);

-- DELETING ROW
DELETE FROM invoices WHERE client_id = (SELECT client_id FROM clients WHERE name = 'Myworks');

-- RESTORING THE DATABASE
-- Note on understanding MySQL Scripting
-- It is much valuable to know SELECT Queries since this will be used always
-- A join (inner) is very common, and always think that the result of the first (base) table will be the basis of the next join in a query that has multiple join statements
-- Left or Right join is just a way to make sure that that we consider not just the condition of the WHERE clause that we set but all DATA on the left/right side
-- Subqueries are just Select statement result. The way we read this is: read first the subquery result to understand what is the condition that has been set by the engineer
